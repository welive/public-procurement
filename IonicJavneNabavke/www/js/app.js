﻿angular.module('starter', ['ionic', 'starter.directives', 'starter.controllers', 'starter.services', 'angularMoment', 'chart.js', 'LocalStorageModule','ionic-modal-select'])

.run(function ($ionicPlatform, $ionicPopup, $http) {
    $ionicPlatform.ready(function () {
        if (cordova.platformId === 'ios' && window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.cordova && window.cordova.InAppBrowser) {
            window.open = window.cordova.InAppBrowser.open;
            $http.post(
                'http://welive.nsinfo.co.rs/javneNabavke/nabavke/log',
                {
                    "msg": "app started",
                    "appId": "javnenabavke",
                    "type": "AppStarted",
                    "custom": { "appname": "PublicProcurement" }
                }
            );
        }

        if (window.StatusBar) {
            StatusBar.styleDefault();
        }

        if (window.Connection) {
            if (navigator.connection.type == Connection.NONE) {
                $ionicPopup.alert({
                    title: "Проблем са повезивањем",
                    content: "Проверите повезивање уређаја са интернетом."
                })
                .then(function (result) {
                    ionic.Platform.exitApp();
                });
            }
        }


    });
})

.config(function ($stateProvider, $urlRouterProvider, localStorageServiceProvider) {

    localStorageServiceProvider
      .setPrefix('javneNabavke')
      .setStorageType('localStorage');

    $stateProvider
      .state('app', {
          url: '/app',
          abstract: true,
          templateUrl: 'templates/menu.html',
          controller: 'AppCtrl'
      })
        .state('app.terms', {
            url: '/terms',
            views: {
                'menuContent': {
                    templateUrl: 'templates/terms.html',
                    controller: 'TermsCtrl'
                }
            }
        })
    .state('app.list', {
        url: '/list',
        views: {
            'menuContent': {
                templateUrl: 'templates/list.html'
            }
        }
    })
    .state('app.detail', {
        url: '/detail/:nabavkaId',
        views: {
            'menuContent': {
                templateUrl: 'templates/detail.html'
            }
        }
    })
    .state('app.about', {
        url: '/about',
        views: {
            'menuContent': {
                templateUrl: 'templates/about.html'
            }
        }
    })
    .state('app.statistics', {
        url: '/statistics',
        views: {
            'menuContent': {
                templateUrl: 'templates/statistics.html'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/about');
});

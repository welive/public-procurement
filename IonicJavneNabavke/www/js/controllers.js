﻿angular.module('starter.controllers', [])

.controller('AppCtrl', function ($scope, $ionicModal, $timeout) {

    $scope.loginData = {};
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.closeLogin = function () {
        $scope.modal.hide();
    };

    $scope.login = function () {
        $scope.modal.show();
    };

    $scope.doLogin = function () {
        console.log('Doing login', $scope.loginData);

        $timeout(function () {
            $scope.closeLogin();
        }, 1000);
    };
})

.controller('NabavkeListCtrl', function ($http, $scope, $ionicLoading, $ionicModal, JavneNabavkeData, $ionicPopup) {
    $scope.nabavke = [];
    $scope.pagination = {};
    $scope.mesta = [];
    $scope.filter = {
        mesto: null,
        vrstaPostupka: null,
        kategorijaNarucioca: null
    };

    $scope.haveNabavke = true;


    function loadDataForList() {
        if ($scope.nabavke.length > 0 && $scope.pagination.next === undefined) {
            $scope.haveNabavke = false;
        } else {
            JavneNabavkeData.getPagination($scope.nabavke.length, $scope.pagination.next, 255, $scope.nabavke.length > 0 ? $scope.pagination.next : 0, $scope.filter, function (retData) {

                angular.forEach(retData.nabavke, function (data) {
                    $scope.nabavke.push(data);
                });
                $scope.pagination = retData.pagination;


                if ($scope.nabavke.length === 0) {
                    $scope.haveNabavke = false;
                    $ionicPopup.alert({
                        title: 'Обавештење',
                        template: 'Нема података за задати критеријум.'
                    });
                } else {
                    $scope.haveNabavke = true;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }

            });
        }
    }

    $scope.loadOlder = loadDataForList;

    $scope.getMestoSelect = function (newValue, oldValue) {
        $scope.filter.mesto = newValue;
    };

    $scope.getVrstaPostupkaSelect = function (newValue, oldValue) {
        $scope.filter.vrstaPostupka = newValue;
    };

    $scope.getKategorijaNaruciocaSelect = function (newValue, oldValue) {
        $scope.filter.kategorijaNarucioca = newValue;
    };



    $ionicModal.fromTemplateUrl('templates/filter.html', {
        scope: $scope
    }).then(function (modal) {

        $scope.modal = modal;
    });

    $scope.openModal = function () {
        var mesta = [];
        var vrstaPostupka = [];
        var kategorijaNarucioca = [];

        JavneNabavkeData.getMesta(function (retData) {
            angular.forEach(retData, function (data) {
                mesta.push(data);
            });
            $scope.mesta = mesta;
        });

        JavneNabavkeData.getVrstaPostupka(function (retData) {
            angular.forEach(retData, function (data) {
                vrstaPostupka.push(data);
            });
            $scope.vrstaPostupka = vrstaPostupka;
        });

        JavneNabavkeData.getKategorijaNarucioca(function (retData) {
            angular.forEach(retData, function (data) {
                kategorijaNarucioca.push(data);
            });
            $scope.kategorijaNarucioca = kategorijaNarucioca;
        });

        $http.post(
                'http://welive.nsinfo.co.rs/javneNabavke/nabavke/log',
                {
                    "msg": "app personalized",
                    "appId": "publicprocurement",
                    "type": "AppPersonalize",
                    "custom": { "appname": "PublicProcurement" }
                }
            );

        $scope.modal.show();
    };

    $scope.closeModal = function () {
        $scope.modal.hide();
    };

    $scope.createFilter = function () {
        $scope.nabavke = [];
        loadDataForList();
        $scope.modal.hide();
    };
})

.controller('DetailCtrl', function ($http, $scope, $stateParams, JavneNabavkeData) {
    $scope.data = {};
    $scope.nabavkaId = $stateParams.nabavkaId;

    $scope.goToNabavku = function (url) {
        window.open(url, '_blank', 'location=no,status=0');
    };

    getNabavka($stateParams.nabavkaId);

    function getNabavka(id) {
        $http.post(
               'http://welive.nsinfo.co.rs/javneNabavke/nabavke/log',
               {
                   "msg": "app personalized",
                   "appId": "javnenabavke",
                   "type": "ProcurementDetailSeen",
                   "custom": { "procurementid": id, "appname": "PublicProcurement" }
               }
           );
        JavneNabavkeData.getNabavka(id, function (response) {
            $scope.data = response;
        });

    }
})

.controller('StatCtrl', function ($scope, $stateParams, $http, JavneNabavkeData) {

    $scope.options = {
        responsive: true,
        maintainAspectRatio: true
    }

    $scope.data = [[]];
    $scope.labels = [];
    $scope.options = {
        animation: false,
        showScale: true,
        showTooltips: false,
        scaleSteps: 10,
        scaleStepWidth: Math.ceil(2000 / 50),
        scaleStartValue: 0,
        pointDot: false,
        datasetStrokeWidth: 0.5
    };
    getTop10Mesta();
    function getTop10Mesta() {
        JavneNabavkeData.getTop10Mesta(function (result) {
            $scope.data[0] = result.data;
            $scope.labels = result.labels;
        });
    };



})
.controller('AboutCtrl', function ($scope, $state, $ionicPopup, localStorageService) {
    var isPrivacyAccepted = localStorageService.get("isPrivacyAccepted");
    if (!isPrivacyAccepted) {
        $state.go('app.terms');
    }

    $scope.goToPoll = function () {
        console.log('Poll');
        var appName = encodeURIComponent("PublicProcurement");
        var pilotName = encodeURIComponent("Novisad");
        var poll = window.open("http://in-app.cloudfoundry.welive.eu/html/index.html?app=" + appName + "&pilotId=" + pilotName + "&callback=http://localhost/callback&lang=SR", '_blank', 'location=no');
        poll.addEventListener('loadstart', function (event) {
            console.log(event.url);
            if ((event.url).startsWith("http://localhost/callback")) {
                retValue = (event.url).split("questionnaire-status=")[1];
                if (retValue === 'OK') {
                    console.log('OK');
                    localStorageService.set("poll", true);
                    poll.close();
                    $ionicPopup.alert({
                        title: "Анкета",
                        content: "Хвала што сте попунили анкету.Ваше мишљење нам је важно."
                    })
                } else if (retValue === 'CANCEL') {
                    console.log('CANCEL');
                    poll.close();
                    $ionicPopup.alert({
                        title: "Анкета",
                        content: "Ваше мишљење нам је важно. Жао нам је што сте одустали од анкете."
                    })
                }
                else {
                    console.log('ERROR');
                }
            }
        });
    };
})
.controller('TermsCtrl', function (
    $http,
    $scope,
    $ionicHistory,
    $state,
    localStorageService,
    $ionicPopup,
    $filter,
    $timeout,
    $ionicSideMenuDelegate) {

    // Avoid dragging content to show side menu and go to another page without accepting the privacy policy
    $ionicSideMenuDelegate.canDragContent(false);

    var isPrivacyAccepted = localStorageService.get("isPrivacyAccepted"); //window.localStorage.getItem("isPrivacyAccepted");



    // go to the app's first page
    $scope.goToProposalsList = function () {
        // Avoid back button in the next view
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('app.about');
    };

    $scope.acceptPrivacy = function () {
        localStorageService.set("isPrivacyAccepted", true); //window.localStorage.setItem("isPrivacyAccepted", true);
        $scope.goToProposalsList();
    };

    $scope.refusePrivacy = function () {
        var myPopup = $ionicPopup.show({
            template: "<center>Жао нам је што сте одустали од коришћења апликације.</center>",
            cssClass: 'custom-class custom-class-popup'
        });
        $timeout(function () { myPopup.close(); }, 1800) //close the popup after 1.8 seconds for some reason
            .then(function () {
                navigator.app.exitApp(); // sometimes doesn't work with Ionic View
                ionic.Platform.exitApp();
                console.log('App closed');
            });
    };

    if (isPrivacyAccepted) $scope.goToProposalsList();

});


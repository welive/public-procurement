﻿var service = angular.module('starter.services', [])
service.factory('JavneNabavkeData', ['$http', function ($http, $httpParamSerializer) {

    var JavneNabavkeData = {};

    JavneNabavkeData.getPagination = function (num, next, npp, page,filter,cb) {
        var returnData = {};

        var params = { npp: 25, page: num > 0 ? next : 0 };

        if (filter.mesto) {
            params['mesto'] = filter.mesto.id;
        }

        if (filter.vrstaPostupka) {
            params['vrstaPostupka'] = filter.vrstaPostupka.id;
        }

        if (filter.kategorijaNarucioca) {
            params['kategorijaNarucioca'] = filter.kategorijaNarucioca.id;
        }

        var url = 'http://welive.nsinfo.co.rs/javneNabavke/';

        $http.get(url, {params:params}).success(function (response) {
            var nabavke = [];
            angular.forEach(response.results, function (data) {
                nabavke.push(data);
            });
            var pagination = response.pagination;
            returnData = {
                nabavke: nabavke,
                pagination: pagination
            }
            cb(returnData);
        });
    };

    JavneNabavkeData.getNabavka = function (id, cb) {
        var url = 'http://welive.nsinfo.co.rs/javneNabavke/nabavka/' + id;
        $http.get(url)
        .success(function (response) {
            cb(response[0]);
        });
    };

    JavneNabavkeData.getMesta = function (cb) {
        var url = 'http://welive.nsinfo.co.rs/javneNabavke/nabavke/getMesta';
        $http.get(url)
        .success(function (response) {
            cb(response);
        });
    };

    JavneNabavkeData.getVrstaPostupka = function (cb) {
        var url = 'http://welive.nsinfo.co.rs/javneNabavke/nabavke/getVrstaPostupka';
        $http.get(url)
        .success(function (response) {
            cb(response);
        });
    };

    JavneNabavkeData.getKategorijaNarucioca = function (cb) {
        var url = 'http://welive.nsinfo.co.rs/javneNabavke/nabavke/getKategorijaNarucioca';
        $http.get(url)
        .success(function (response) {
            cb(response);
        });
    };

    JavneNabavkeData.getTop10Mesta = function (cb) {
        var url = 'http://welive.nsinfo.co.rs/javneNabavke/nabavke/top10mesta';
        var result = { data: [], labels: [] };
        $http.get(url)
        .success(function (response) {
            for (var i = 0; i < response.data.length; i++) {
                result.data.push(response.data[i]);
                result.labels.push(response.mesta[i]);
            }
            cb(result);
        });
    }

    return JavneNabavkeData;

}]);